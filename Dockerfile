FROM openjdk:17-jdk

COPY target/management-training.jar .

EXPOSE 8081 

ENTRYPOINT [ "java", "-jar", "management-training.jar"]
