package io.management.exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

interface SumCalculator
{
	int sum(int a, int b);
}
interface EmptyString
{
	boolean isEmpti(String s);
}

interface OpStringhe
{
	String concatena(String s1, String s2);
}

interface WordCounter{
	int  countWords(String sentence);
}


	public class StreamExercise {
		public static boolean isPalindrome(String str)
		{
			String reversed = new StringBuilder(str).reverse().toString();
			return str.equals(reversed);
		}
		
		public static List<Integer> filterPrimeNumbers(List<Integer> numbers)
		{
			Predicate<Integer> isPrime = number -> {
				if (number <= 1)
				{
					return false;
				}
				for (int i = 2; i <= Math.sqrt(number); i++)
				{
					if (number % i == 0)
					{
						return false;
					}
				}
				return true;
			};
	 
			List<Integer> primeNumbers = new ArrayList<>();
			for (int number : numbers)
			{
				if (isPrime.test(number))
				{
					primeNumbers.add(number);
				}
			}
			return primeNumbers;
		}
		
		
		public static void main(String[] args) {
			//1
			SumCalculator calculator = (n1, n2) -> n1 + n2;
			System.out.println("1.\n"+calculator.sum(1, 2));
			
			//2
			List<String> lista = Arrays.asList("Tutor", "Joes", "Computer", "Education");;
			List<String> listaUp = lista.stream().map(s -> s.toUpperCase()).collect(Collectors.toList());
			System.out.println("\n2.\nUpperCase: ");
			listaUp.forEach(s -> System.out.println(s));
			List<String> listaLow = lista.stream().map(s -> s.toLowerCase()).collect(Collectors.toList());
			System.out.println("LowerCase: ");
			listaLow.forEach(s -> System.out.println(s));
			
			//3
			List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);
			List<Integer> evens = integers.stream().filter(n -> n%2==0).collect(Collectors.toList());
			System.out.println("\n3.\nEvens: ");
			evens.forEach(s -> System.out.println(s));
			
			//4
			List<Integer> odds = integers.stream().filter(n -> n%2==1).collect(Collectors.toList());
			System.out.println("\n4.\nOdds: ");
			odds.forEach(s -> System.out.println(s));
			
			//5
			String prova = "";
			EmptyString test = s -> s.isEmpty();
			System.out.println("\n5.\n"+test.isEmpti(prova));
			
			//6
			List<Double> doubles = Arrays.asList(1.55 , 2.5, 3.6, 4.7, 5.3);
			double sum = doubles.stream().mapToDouble(n -> n.doubleValue()).sum();
			double average = sum/doubles.size();
			System.out.println("\n6.\n" + average);
			
			//7
			List<Integer> duplicates = Arrays.asList(1, 2, 3, 4, 5, 4, 3, 5);
			List<Integer> noDuplicate = duplicates.stream().distinct().collect(Collectors.toList());
			System.out.println("\n7.");
			noDuplicate.forEach(n -> System.out.print(n + " "));
			
			//8
			List<String> noOrder = Arrays.asList("Tutor", "Joes", "Computer", "Education");;
			List<String> order = noOrder.stream().sorted().collect(Collectors.toList());
			System.out.println("\n\n8.");
			order.forEach(s -> System.out.println(s));
			
			//9
			List<Integer> listOfNumber = Arrays.asList(1, 2, 3, 4, 5, 8, 7);
			int sumEven = listOfNumber.stream().filter(n -> n%2==0).mapToInt(Integer::intValue).sum();
			System.out.println("\n9.\nSum: "+ sumEven);
			
			//10
			int sumOdd = listOfNumber.stream().filter(n -> n%2==1).mapToInt(Integer::intValue).sum();
			System.out.println("\n10.\nSum: "+ sumOdd);
			
			//11
			int num = 4;
			IntUnaryOperator factorial = n -> {
				int result = 1;
				for (int i = 1; i <= n; i++) {
					result *= i;
				}
				return result;
			};
			
			int fact = factorial.applyAsInt(num);
			System.out.println("\n11.\nFact di "+num+": "+ fact);

			//12
			String s1 = "bau";
			String s2 = "caiiai";
			OpStringhe stringaconcat = (str, str2) -> str+ " " +str2;
			String conStr = stringaconcat.concatena(s1, s2);
			System.out.println("\n12.\n"+conStr);
			
			//13
			List<Integer> numberList = Arrays.asList(13, 21, 78, 23, 35, 8, 1);
			 
			int maxNum = numberList.stream().max(Integer::compare).orElse(-1);
			
			if(maxNum == -1)
				System.out.println("Numbers Not Found");
			else
				System.out.println("\n13.\nMax: "+maxNum);

			//14 Min uguale al 13 ma con min()
			
			//15
			int mul = numberList.stream().reduce(1, (a, b) -> a * b); // Multiply all elements together
			 
			System.out.println("\n15.\nMultiplication of Elements : " + mul);
	 
			int summ = numberList.stream().reduce(0, (a, b) -> a + b); // Sum all elements
	 
			System.out.println("Sum of Elements : " + summ);
			
			//16
			System.out.println("\n16.\nSquared:");
			List<Integer> squaredNumber = numberList.stream().map(n -> n*n).collect(Collectors.toList());
			squaredNumber.forEach(s -> System.out.println(s));

			//17
			System.out.println("\n17.\nIspalindroma:");

			String strPal = "bubub";
			StreamExercise.isPalindrome(strPal);
				
			//18
			System.out.println("\n18.\nLista ordinata per length:");

			List<String> palindList = Arrays.asList("Jaja", "U", "Pasta", "Cars", "Anna", "Arcacra", "Asdrubale");

			Stream<String> orderlength = palindList.stream()
					.sorted(Comparator.comparing(s -> s.length()));
			orderlength.forEach(s -> System.out.println(s));
			
			//19
			System.out.println("\n19.\nMax stringa:");

			String maxString = palindList.stream().max(Comparator.comparing(s -> s.length())).orElse(null);
			System.out.println(maxString);
			
			//20
			System.out.println("\n20.\nMin stringa:");

			String minString = palindList.stream().min(Comparator.comparing(s -> s.length())).orElse(null);
			System.out.println(minString);
			
			//21 Write a Java program using Lambda Expression to convert a list of strings to uppercase
			System.out.println("\n21.\nUpper stringhe:");
			List<String> upperList = palindList.stream().map(s -> s.toUpperCase()).collect(Collectors.toList());
			System.out.println(upperList);
			
			//22 Lambda Expression to filter out all strings from a list that have a length greater than 5 characters
			System.out.println("\n22.\nGreater 5:");
			List<String> greaterLeng = palindList.stream().filter(s -> s.length() > 5).collect(Collectors.toList());
			System.out.println(greaterLeng);
			
			//23 Lambda Expression to convert a list of integers to their corresponding binary strings
			System.out.println("\n23.\ntoBinaryString:");
			List<String> intToBinary = numberList.stream().map(Integer::toBinaryString).collect(Collectors.toList());
			System.out.println(intToBinary);
			
			//24 Lambda Expression to find the prime numbers in a list of integers
			System.out.println("\n24.\nMin stringa:");
			List<Integer> primeNumbers = filterPrimeNumbers(numberList);
			System.out.println(primeNumbers);
			
			//25 Lambda Expression to count the number of words in a given sentence (words are separated by spaces)
			System.out.println("\n25.\nNumber words:");
			String sentence = "ciao sono giovanni e bevo il panino, forse devo dire che sono peppino";
			WordCounter counter = s -> sentence.split("\\s+").length;
			System.out.println(counter.countWords(sentence));
			
			//26 Lambda Expression to convert a list of strings to uppercase if the string length is even and to lowercase if the string length is odd
			System.out.println("\n26.\nEven Upp, Odd Low:");
			List<String> uppAndLow = palindList.stream()
					.map(s -> (s.length() % 2 == 0) ? s.toUpperCase() : s.toLowerCase()).collect(Collectors.toList());
			System.out.println(uppAndLow);
			
			//27 Lambda Expression to find the square of each odd number in a list of integers
			System.out.println("\n27.\nSquare odd numbers:");
			List<Integer> squaredOddList = numberList.stream().filter(n -> n % 2 !=0).map(n -> n*n ).collect(Collectors.toList());
			System.out.println(squaredOddList);
			
			//28 Lambda Expression to calculate the sum of all prime numbers in a list of integers
			System.out.println("\n28.\nPrime numbers sum:");
			int sumPrime = filterPrimeNumbers(numberList).stream().mapToInt(Integer::intValue).sum();
			System.out.println(sumPrime);
			
			//29 Lambda Expression to remove the vowels from a list of strings
			System.out.println("\n29.\nNo Vowel list:");
			List<String> noVowel = palindList.stream()
					.map(s -> s.replaceAll("[AEIOUaeiou]", "")).collect(Collectors.toList());
			System.out.println(noVowel);
			
			//30 Lambda Expression to calculate the product of all even numbers in a list of integers
			System.out.println("\n30.\nProdotto numeri ineteri:");
			int productEven = numberList.stream().filter(n -> n % 2 ==0).reduce(1, (a, b) -> a*b);
			System.out.println(productEven);
			
			//31 Lambda Expression to find the sum of squares of all numbers from 1 to 10
			System.out.println("\n31.\nSum Squares:");
			int sumSquares = IntStream.rangeClosed(1, 10).map(n -> n*n).sum();
			System.out.println(sumSquares);
			
			//32 Lambda Expression to find the difference between the maximum and minimum values in a list of doubles
			System.out.println("\n32.\nDiff max min doubles:");
			double diffMaxMin = doubles.stream().max(Double::compare).get() - doubles.stream().min(Double::compare).get();
			System.out.println(diffMaxMin);
			
			//
			
			//First non-empty string 
			String firstNonemptyString = palindList.stream().filter(s -> !s.isEmpty()).findFirst().orElse("No non-empty string found");
			System.out.println(firstNonemptyString);
			
			//Lambda Expression to check if all strings in a list start with an uppercase letter
			boolean uppercase_start = palindList.stream().allMatch(s -> Character.isUpperCase(s.charAt(0)));
			System.out.println("All the strings start with an uppercase letter? "+uppercase_start);
			
			//
			int sortedSecondNumber = numberList.stream().distinct().sorted(Comparator.reverseOrder()).skip(1).findFirst().get();
			System.out.println("Second largest number: "+sortedSecondNumber);
			
			//45
			//List<String> palindList = Arrays.asList("Jaja", "U", "Pasta", "Cars", "Anna", "Arcacra");;
			String smallest = palindList.stream().filter(StreamExercise:: isPalindrome)
					.min(Comparator.comparing(s -> s.length())).orElse("No palindrome found");
			
			System.out.println("\n45.\n"+"The smallest palindrome string is:" + smallest);




		
		
	}
}
