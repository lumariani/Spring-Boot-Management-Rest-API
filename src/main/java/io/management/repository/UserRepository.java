package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.management.model.User;


public interface UserRepository extends JpaRepository<User, Long>{

	public boolean findByPasswordAndName(String name, String pass);
	
	//public User findByName(String name);
}
