package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.management.model.CourseDate;


public interface CourseDateRepository extends JpaRepository<CourseDate, Long>{

}
