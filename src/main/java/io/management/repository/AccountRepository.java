package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.management.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{

	
}
