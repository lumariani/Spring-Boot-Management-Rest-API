package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.management.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>{

}
