package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.management.model.Platform;

@Repository
public interface PlatformRepository extends JpaRepository<Platform, Long>{

}
