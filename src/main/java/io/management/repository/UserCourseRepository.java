package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.management.model.UserCourse;

@Repository
public interface UserCourseRepository extends JpaRepository<UserCourse, Long>{

	//UserCourse findByUser(User u);
	
}
