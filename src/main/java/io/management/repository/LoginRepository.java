package io.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.management.model.Login;

public interface LoginRepository extends JpaRepository<Login, Long> {

}
