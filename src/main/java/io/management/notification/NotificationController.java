package io.management.notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v1/")
public class NotificationController {

	@Autowired
    private EmailService emailService;
	
	@PostMapping("/send-email")
	public EmailMessage sendEmail(@RequestBody EmailMessage message) {
	    
	    	String emailContent = "Ecco le tue credenziali: " + message.getMessage();
	    	emailService.sendSimpleMessage(message.getTo(), message.getSubject(), emailContent);
	        return message;
//	        // Invia notifica WebSocket
//	        messagingTemplate.convertAndSendToUser(message.getTo(), "/topic/notifications", message);
	
	}
	

//	 @Autowired
//	 private SimpMessagingTemplate messagingTemplate;
//	
//    @MessageMapping("/notify")
//    @SendTo("/topic/notifications")
//    public void send(Notification message) {
//    
////    	String emailContent = "Ecco le tue credenziali: " + message.getMessage();
////    	emailService.sendSimpleMessage(message.getTo(), "New Notification", emailContent);
////        
//        // Invia notifica WebSocket
//        messagingTemplate.convertAndSendToUser(message.getTo(), "/topic/notifications", message);
//
//    }
}