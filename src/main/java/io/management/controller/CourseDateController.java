package io.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.CourseDate;
import io.management.service.CourseDateService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v1/")
public class CourseDateController {

	@Autowired
	private CourseDateService courseDateService;
	
	// get all CourseDates
	@GetMapping("/courseDates")
	public List<CourseDate> getAllCourseDates(){
//		try {
//			return courseDateService.findAllCourseDates();
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting all courseDates", e);
//		}
		return courseDateService.findAllCourseDates();
	}		
	
	// create CourseDate rest api
	@PostMapping("/courseDates")
	@ResponseStatus(HttpStatus.CREATED)
	public CourseDate createCourseDate(@RequestBody CourseDate courseDate) {
//		try {
//			return courseDateService.createCourseDate(courseDate);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating courseDate", e);
//		}
		return courseDateService.createCourseDate(courseDate);
	}
	
	// get CourseDate by id rest api
	@GetMapping("/courseDates/{id}")
	public CourseDate getCourseDateById(@PathVariable Long id) {
//		try {
//			return courseDateService.getCourseDateById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting courseDate by id", e);
//		}
		return courseDateService.getCourseDateById(id);
	}
	
	// update CourseDate rest api
	
	@PutMapping("/courseDates/{id}")
	public CourseDate updateCourseDate(@PathVariable Long id, @RequestBody CourseDate courseDateDetails){
//		try {
//			return courseDateService.updateCourseDate(id, courseDateDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all courseDates", e);
//		}
		return courseDateService.updateCourseDate(id, courseDateDetails);
	}
	
	// delete CourseDate rest api
	@DeleteMapping("/courseDates/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCourseDate(@PathVariable Long id){
//		try {
//			courseDateService.deleteCourseDate(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting courseDate", e);
//		}
		courseDateService.deleteCourseDate(id);
	}
}
