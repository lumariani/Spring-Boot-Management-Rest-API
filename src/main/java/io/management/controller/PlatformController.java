package io.management.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.Platform;
import io.management.service.PlatformService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v1/")
public class PlatformController {

	@Autowired
	private PlatformService platformService;
	
	// get all Platforms
	@GetMapping("/platforms")
	public List<Platform> getAllPlatforms(){
//		try {
//			return platformService.findAllPlatforms();
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting all platforms", e);
//		}
		return platformService.findAllPlatforms();
	}		
	
	// create Platform rest api
	@PostMapping("/platforms")
	@ResponseStatus(HttpStatus.CREATED)
	public Platform createPlatform(@RequestBody Platform platform) {
//		try {
//			return platformService.createPlatform(platform);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating platform", e);
//		}
		return platformService.createPlatform(platform);
	}
	
	// get Platform by id rest api
	@GetMapping("/platforms/{id}")
	public Platform getPlatformById(@PathVariable Long id) {
//		try {
//			return platformService.getPlatformById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting platform by id", e);
//
//		}	
		return platformService.getPlatformById(id);
	}
	
	// update Platform rest api
	
	@PutMapping("/platforms/{id}")
	public Platform updatePlatform(@PathVariable Long id, @RequestBody Platform platformDetails){
//		try {
//			return platformService.updatePlatform(id, platformDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all platforms", e);
//		}
		return platformService.updatePlatform(id, platformDetails);
	}
	
	// delete Platform rest api
	@DeleteMapping("/platforms/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePlatform(@PathVariable Long id){
//		try {
//			platformService.deletePlatform(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting platform", e);
//		}
		platformService.deletePlatform(id);
	}
}
