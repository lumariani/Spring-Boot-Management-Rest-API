package io.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.Login;
import io.management.service.LoginService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v1/")
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	// get all Logins
	@GetMapping("/logins")
	public List<Login> getAllLogins(){
		/*
		 * try { return loginService.findAllLogins(); } catch (Exception e) { throw
		 * new RuntimeException("Http Error getting all logins", e); }
		 */
		return loginService.findAllLogins();
	}		
	
	// create Login rest api
	@PostMapping("/logins")
	@ResponseStatus(HttpStatus.CREATED)
	public Login createLogin(@RequestBody Login login) {
//		try {
//			return loginService.createLogin(login);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating login", e);
//		}
		return loginService.createLogin(login);
	}
	
	// get Login by id rest api
	@GetMapping("/logins/{id}")
	public Login getLoginById(@PathVariable Long id) {
//		try {
//			return loginService.getLoginById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting login by id", e);
//		}
		return loginService.getLoginById(id);
	}
	
	// update Login rest api
	
	@PutMapping("/logins/{id}")
	public Login updateLogin(@PathVariable Long id, @RequestBody Login loginDetails){
//		try {
//			return loginService.updateLogin(id, loginDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all logins", e);
//		}
		return loginService.updateLogin(id, loginDetails);
	}
	
	// delete Login rest api
	@DeleteMapping("/logins/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteLogin(@PathVariable Long id){
//		try {
//			loginService.deleteLogin(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting login", e);
//		}
		loginService.deleteLogin(id);
	}
}
