package io.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.Account;
import io.management.service.AccountService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v1/")
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	// get all Accounts
	@GetMapping("/accounts")
	public List<Account> getAllAccounts(){
		/*
		 * try { return accountService.findAllAccounts(); } catch (Exception e) { throw
		 * new RuntimeException("Http Error getting all accounts", e); }
		 */
		return accountService.findAllAccounts();
	}		
	
	// create Account rest api
	@PostMapping("/accounts")
	//@ResponseStatus(HttpStatus.CREATED)
	public Account createAccount(@RequestBody Account account) {
//		try {
//			return accountService.createAccount(account);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating account", e);
//		}
		return accountService.createAccount(account);
	}
	
	// get Account by id rest api
	@GetMapping("/accounts/{id}")
	public Account getAccountById(@PathVariable Long id) {
//		try {
//			return accountService.getAccountById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting account by id", e);
//		}
		return accountService.getAccountById(id);
	}
	
	// update Account rest api
	
	@PutMapping("/accounts/{id}")
	public Account updateAccount(@PathVariable Long id, @RequestBody Account accountDetails){
//		try {
//			return accountService.updateAccount(id, accountDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all accounts", e);
//		}
		return accountService.updateAccount(id, accountDetails);
	}
	
	// delete Account rest api
	@DeleteMapping("/accounts/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAccount(@PathVariable Long id){
//		try {
//			accountService.deleteAccount(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting account", e);
//		}
		accountService.deleteAccount(id);
	}
}
