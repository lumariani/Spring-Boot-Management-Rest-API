package io.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.UserCourse;
import io.management.service.UserCourseService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v1/")
public class UserCourseController {

	@Autowired
	private UserCourseService userCourseService;
	
	// get all UserCourses
	@GetMapping("/userCourses")
	public List<UserCourse> getAllUserCourses(){
		/*
		 * try { return userCourseService.findAllUserCourses(); } catch (Exception e) {
		 * throw new RuntimeException("Http Error getting all userCourses", e); }
		 */
		return userCourseService.findAllUserCourses();
	}		
	
	// create UserCourse rest api
	@PostMapping("/userCourses")
	@ResponseStatus(HttpStatus.CREATED)
	public UserCourse createUserCourse(@RequestBody UserCourse userCourse) {
//		try {
//			return userCourseService.createUserCourse(userCourse);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating userCourse", e);
//		}
		return userCourseService.createUserCourse(userCourse);
	}
	
	// get UserCourse by id rest api
	@GetMapping("/userCourses/{id}")
	public UserCourse getUserCourseById(@PathVariable Long id) {
//		try {
//			return userCourseService.getUserCourseById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting userCourse by id", e);
//		}
		return userCourseService.getUserCourseById(id);
	}
	
//	@GetMapping("/userCourses/{user}")
//	public UserCourse getUserCourseByUser(@PathVariable User user) {
////		try {
////			return userCourseService.getUserCourseById(id);
////		} catch (Exception e) {
////			throw new RuntimeException("Http Error getting userCourse by id", e);
////		}
//		return userCourseService.getUserCourseByUser(user);
//	}
	
	// update UserCourse rest api
	
	@PutMapping("/userCourses/{id}")
	public UserCourse updateUserCourse(@PathVariable Long id, @RequestBody UserCourse userCourseDetails){
//		try {
//			return userCourseService.updateUserCourse(id, userCourseDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all userCourses", e);
//		}
		return userCourseService.updateUserCourse(id, userCourseDetails);
	}
	
	// delete UserCourse rest api
	@DeleteMapping("/userCourses/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUserCourse(@PathVariable Long id){
//		try {
//			userCourseService.deleteUserCourse(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting userCourse", e);
//		}
		userCourseService.deleteUserCourse(id);
	}
}
