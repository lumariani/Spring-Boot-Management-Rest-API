package io.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.Course;
import io.management.service.CourseService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v2/")
public class CourseInfoController {

	@Autowired
	private CourseService courseService;
	
	// get all Courses
	@GetMapping("/courses")
	public List<Course> getAllCourses(){
//		try {
//			return courseService.findAllCourses();
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting all courses", e);
//		}
		return courseService.findAllCourses();
	}		
	
	// create Course rest api
	@PostMapping("/courses")
	@ResponseStatus(HttpStatus.CREATED)
	public Course createCourse(@RequestBody Course course) {
//		try {
//			return courseService.createCourse(course);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating course", e);
//		}
		return courseService.createCourse(course);
	}
	
	// get Course by id rest api
	@GetMapping("/courses/{id}")
	public Course getCourseById(@PathVariable Long id) {
//		try {
//			return courseService.getCourseById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting course by id", e);
//		}
		return courseService.getCourseById(id);
	}
	
	// update Course rest api
	
	@PutMapping("/courses/{id}")
	public Course updateCourse(@PathVariable Long id, @RequestBody Course courseDetails){
//		try {
//			return courseService.updateCourse(id, courseDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all courses", e);
//		}
		return courseService.updateCourse(id, courseDetails);
	}
	
	// delete Course rest api
	@DeleteMapping("/courses/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCourse(@PathVariable Long id){
//		try {
//			courseService.deleteCourse(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting course", e);
//		}
		courseService.deleteCourse(id);
	}
}

