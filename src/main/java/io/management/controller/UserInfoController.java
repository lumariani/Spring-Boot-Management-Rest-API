package io.management.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.management.model.User;
import io.management.service.UserService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/academia/v2/")
public class UserInfoController {
	
	@Autowired
	private UserService userService;
	
	//@MessageMapping()
	//@SendTo()
	// get all Users
	@GetMapping("/users/info")
	public List<User> getAllUsers(){
//		try {
//			return userService.findAllUsers();
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting all users", e);
//		}
		return userService.findAllUsers();
	}		
	
	
//	@PostMapping("/login")
//	public User findByName(@RequestBody String name) {
//		 return userService.findByName(name);
//	}
	
	// create User rest api
	@PostMapping("/users")
	@ResponseStatus(HttpStatus.CREATED)
	public User createUser(@RequestBody User user) {
//		try {
//			return userService.createUser(user);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error creating user", e);
//
//		}
		return userService.createUser(user);
	}
	
	// get User by id rest api
	@GetMapping("/users/info/{id}")
	public User getUserById(@PathVariable Long id) {
//		try {
//			return userService.getUserById(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error getting user by id", e);
//		}
		return userService.getUserById(id);
	}
	
	// update User rest api
	
	@PutMapping("/users/info/{id}")
	public User updateUser(@PathVariable Long id, @RequestBody User userDetails){
//		try {
//			return userService.updateUser(id, userDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all users", e);
//		}
		return userService.updateUser(id, userDetails);
	}
	
	@PutMapping("/users/info/{userId}/history/{courseId}")
	public User updateUserHistory(@PathVariable Long userId, @PathVariable Long courseId){
//		try {
//			return userService.updateUser(id, userDetails);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error updating all users", e);
//		}
		return userService.updateUserHistory(userId, courseId);
	}
	
	// delete User rest api
	@DeleteMapping("/users/info/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable Long id){
//		try {
//			userService.deleteUser(id);
//		} catch (Exception e) {
//			throw new RuntimeException("Http Error deleting user", e);
//		}
		userService.deleteUser(id);
	}
	
	@DeleteMapping("/users/info/{userId}/history/{courseDateId}")
    public void removeCourseDateFromUser(@PathVariable long userId,
            @PathVariable long courseDateId) {
        userService.removeUserHistoryCourse(userId, courseDateId);
    }

}

