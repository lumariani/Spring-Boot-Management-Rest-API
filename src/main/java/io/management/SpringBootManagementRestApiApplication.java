package io.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootManagementRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootManagementRestApiApplication.class, args);
	}

}
