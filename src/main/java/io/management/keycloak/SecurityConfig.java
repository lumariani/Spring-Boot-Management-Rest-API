package io.management.keycloak;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;


import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

	private final JwtConverter jwtConverter;
	
	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
		
		.authorizeHttpRequests((authorize) -> authorize
			 
			//.requestMatchers("/ws").hasRole("client_admin") //it works no need to add Annotation in controller
//			.requestMatchers("/academia/v2/**").hasRole("client_user")
		//.requestMatchers("/academia/v1/**").hasRole("client_admin")
//			
//		  
			//.requestMatchers("/academia/v1/platforms/**").hasRole("client_user")// example, client_user role can access platform CRUD operations
			.anyRequest().authenticated()
		)
		.oauth2ResourceServer((oauth2) -> oauth2
			.jwt(token -> token.jwtAuthenticationConverter(jwtConverter)) //Customizer.withDefaults()
			//.jwtAuthenticationConverter(jwtConverter)
		);
		
//		http.csrf(AbstractHttpConfigurer::disable);
//
//        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
//        
	return http.build();
	}

}




