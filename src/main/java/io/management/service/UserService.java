package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.CourseDate;
import io.management.model.User;
import io.management.repository.CourseDateRepository;

import io.management.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CourseDateRepository courseDateRepository;
	
	public List<User> findAllUsers(){
//		try {
//			return userRepository.findAll();
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching all users", e);
//		}
		return userRepository.findAll();
	}	
	
	public User createUser(User user) {
//		try {
//			return userRepository.save(user);
//		} catch (Exception e) {
//			throw new RuntimeException("Error creating user", e);
//		}
		return userRepository.save(user); 
	}
	
	public User getUserById(Long id) {
//		try {
//			User user = userRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));
//			return user;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching user by id", e);
//		}
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));
		return user;
	}
	
	public User updateUser(Long id, User userDetails){
//		try {
//			User user = userRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));
//			
//			user.setEmail(userDetails.getEmail());
//			
//			User updatedUser = userRepository.save(user);
//			return updatedUser;
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating user id", e);
//		}
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + id));
		
		user.setEmail(userDetails.getEmail());
		user.setName(userDetails.getName());
		user.setSurname(userDetails.getSurname());
		user.setNotification(userDetails.getNotification());
		user.setPassword(userDetails.getPassword());		

		User updatedUser = userRepository.save(user);
		return updatedUser;
	}
	
	public void deleteUser(Long id) {
//		if(!userRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting user: "+ id);
//		}
		userRepository.deleteById(id);
	}

	public boolean findByPasswordAndName(String name, String pass) {
		 userRepository.findByPasswordAndName(name, pass);
		 
		 return false;
		
	}

	public User updateUserHistory(Long userId, Long courseId) {
		List<CourseDate> courses = null;
		
		User user = userRepository.findById(userId).get();
		CourseDate course = courseDateRepository.findById(courseId).get();
		
		courses = user.getCourseHistory();
		
		courses.add(course);
		user.setCourseHistory(courses);
		return userRepository.save(user);
	}
	
	//per eliminare un corso dello storico
	public User removeUserHistoryCourse(Long userId, Long courseId) {
		List<CourseDate> courses = null;
		
		User user = userRepository.findById(userId).get();
		CourseDate course = courseDateRepository.findById(courseId).get();
		
		courses = user.getCourseHistory();
		
		courses.remove(course);
		user.setCourseHistory(courses);
		return userRepository.save(user);
	}
	
//	public User findByName(String name) {
//	    User user = userRepository.findByName(name);
//		return userRepository.save(user);	}
	
}
