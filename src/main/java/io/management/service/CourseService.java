package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.Course;
import io.management.repository.CourseRepository;

@Service
public class CourseService {

	@Autowired
	private CourseRepository courseRepository;
	
	public List<Course> findAllCourses(){
//		try {
//			return courseRepository.findAll();
//		} catch (Exception e) {
//			throw new RuntimeException("Error finding all courses", e);
//		}
		return courseRepository.findAll();

	}	
	
	public Course createCourse(Course course) {
//		try {
//			return courseRepository.save(course);
//		} catch (Exception e) {
//			throw new RuntimeException("Error creating course", e);
//		}
		return courseRepository.save(course);
	}
	
	public Course getCourseById(Long id) {
//		try {
//			Course course = courseRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Course not exist with id :" + id));
//			return course;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching course by id", e);
//		}
		Course course = courseRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Course not exist with id :" + id));
		return course;
	}
	
	public Course updateCourse(Long id, Course courseDetails){
//		try {
//			Course course = courseRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Course not exist with id :" + id));
//			
//			course.setName(courseDetails.getName());
//			course.setLanguage(courseDetails.getLanguage());
//			
//			Course updatedCourse = courseRepository.save(course);
//			return updatedCourse;
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating course", e);
//		}
		Course course = courseRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Course not exist with id :" + id));
		
		course.setName(courseDetails.getName());
		course.setLanguage(courseDetails.getLanguage());
		course.setPlatform(courseDetails.getPlatform());
		
		Course updatedCourse = courseRepository.save(course);
		return updatedCourse;
	}
	
	public void deleteCourse(Long id) {
//		if(!courseRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting course: "+ id);
//		}
		courseRepository.deleteById(id);
	}
}
