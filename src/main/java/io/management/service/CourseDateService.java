package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.CourseDate;
import io.management.repository.CourseDateRepository;

@Service
public class CourseDateService {

	@Autowired
	private CourseDateRepository courseDateRepository;
	
	public List<CourseDate> findAllCourseDates(){
//		try {
//			return courseDateRepository.findAll();
//		} catch (Exception e) {
//			throw new RuntimeException("Error finding all courseDates", e);
//		}
		return courseDateRepository.findAll();

	}	
	
	public CourseDate createCourseDate(CourseDate courseDate) {
//		try {
//			return courseDateRepository.save(courseDate);
//		} catch (Exception e) {
//			throw new RuntimeException("Error creating courseDate", e);
//		}
		return courseDateRepository.save(courseDate);
	}
	
	public CourseDate getCourseDateById(Long id) {
//		try {
//			CourseDate courseDate = courseDateRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("CourseDate not exist with id :" + id));
//			return courseDate;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching courseDate by id", e);
//		}
		CourseDate courseDate = courseDateRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("CourseDate not exist with id :" + id));
		return courseDate;
	}
	
	public CourseDate updateCourseDate(Long id, CourseDate courseDateDetails){
//		try {
//			CourseDate courseDate = courseDateRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("CourseDate not exist with id :" + id));
//			
//			courseDate.setName(courseDateDetails.getName());
//			courseDate.setLanguage(courseDateDetails.getLanguage());
//			
//			CourseDate updatedCourseDate = courseDateRepository.save(courseDate);
//			return updatedCourseDate;
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating courseDate", e);
//		}
		CourseDate courseDate = courseDateRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("CourseDate not exist with id :" + id));
		
		courseDate.setName(courseDateDetails.getName());
		courseDate.setLanguage(courseDateDetails.getLanguage());
		courseDate.setPlatform(courseDateDetails.getPlatform());
		courseDate.setStart(courseDateDetails.getStart());
		courseDate.setEnd(courseDateDetails.getEnd());
		
		CourseDate updatedCourseDate = courseDateRepository.save(courseDate);
		return updatedCourseDate;
	}
	
	public void deleteCourseDate(Long id) {
//		if(!courseDateRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting courseDate: "+ id);
//		}
		courseDateRepository.deleteById(id);
	}
}
