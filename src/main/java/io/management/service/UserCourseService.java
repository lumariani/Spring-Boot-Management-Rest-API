package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.UserCourse;
import io.management.repository.UserCourseRepository;

@Service
public class UserCourseService {

	@Autowired
	private UserCourseRepository userCourseRepository;
	
	public List<UserCourse> findAllUserCourses(){
//		try {
//			return userCourseRepository.findAll();
//		} catch (Exception e){
//			throw new RuntimeException("Error fetching all userCourses", e);
//		}
		return userCourseRepository.findAll();
	}	
	
	public UserCourse createUserCourse(UserCourse userCourse) {
//		try {
//			return userCourseRepository.save(userCourse);
//		}catch (Exception e) {
//			throw new RuntimeException("Error creating userCourse", e);
//		}
		return userCourseRepository.save(userCourse);

	}
	
	public UserCourse getUserCourseById(Long id) {
//		try {
//			UserCourse userCourse = userCourseRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("UserCourse not exist with id :" + id));
//			return userCourse;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching userCourse by id", e);
//		}
		UserCourse userCourse = userCourseRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("UserCourse not exist with id :" + id));
		return userCourse;
	}
	
//	public UserCourse getUserCourseByUser(User u) {
////		try {
////			UserCourse userCourse = userCourseRepository.findById(id)
////					.orElseThrow(() -> new ResourceNotFoundException("UserCourse not exist with id :" + id));
////			return userCourse;
////		} catch (Exception e) {
////			throw new RuntimeException("Error fetching userCourse by id", e);
////		}
//		try {
//		UserCourse userCourse = userCourseRepository.findByUser(u);
//		return userCourse;
//		}
//		catch(Exception e) {
//			throw new ResourceNotFoundException("UserCourse not exist with user id :" + u.getId());
//		}
//	}
	
	public UserCourse updateUserCourse(Long id, UserCourse userCourseDetails){
//		try {
//			UserCourse userCourse = userCourseRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("UserCourse not exist with id :" + id));
//			
//			userCourse.setUserName(userCourseDetails.getUserName());
//			userCourse.setPassword(userCourseDetails.getPassword());
//			
//			UserCourse updatedUserCourse = userCourseRepository.save(userCourse);
//			return updatedUserCourse;
//			
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating userCourse id", e);
//		}
		UserCourse userCourse = userCourseRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("UserCourse not exist with id :" + id));
		
		userCourse.setStart(userCourseDetails.getStart());
		userCourse.setEnd(userCourseDetails.getEnd());
		userCourse.setCourse(userCourseDetails.getCourse());
		userCourse.setUser(userCourseDetails.getUser());
		
		
		UserCourse updatedUserCourse = userCourseRepository.save(userCourse);
		return updatedUserCourse;
	}
	
	public void deleteUserCourse(Long id) {
//		if(!userCourseRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting userCourse: "+ id);
//		}
		userCourseRepository.deleteById(id);

	}
}

