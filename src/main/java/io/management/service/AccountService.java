package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.Account;
import io.management.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	public List<Account> findAllAccounts(){
//		try {
//			return accountRepository.findAll();
//		} catch (Exception e){
//			throw new RuntimeException("Error fetching all accounts", e);
//		}
		return accountRepository.findAll();
	}	
	
	public Account createAccount(Account account) {
//		try {
//			return accountRepository.save(account);
//		}catch (Exception e) {
//			throw new RuntimeException("Error creating account", e);
//		}
		return accountRepository.save(account);

	}
	
	public Account getAccountById(Long id) {
//		try {
//			Account account = accountRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Account not exist with id :" + id));
//			return account;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching account by id", e);
//		}
		Account account = accountRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Account not exist with id :" + id));
		return account;
	}
	
	public Account updateAccount(Long id, Account accountDetails){
//		try {
//			Account account = accountRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Account not exist with id :" + id));
//			
//			account.setUserName(accountDetails.getUserName());
//			account.setPassword(accountDetails.getPassword());
//			
//			Account updatedAccount = accountRepository.save(account);
//			return updatedAccount;
//			
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating account id", e);
//		}
		Account account = accountRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Account not exist with id :" + id));
		
		account.setUserName(accountDetails.getUserName());
		account.setPassword(accountDetails.getPassword());
		account.setPlatform(accountDetails.getPlatform());
		
		Account updatedAccount = accountRepository.save(account);
		return updatedAccount;
	}
	
	public void deleteAccount(Long id) {
//		if(!accountRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting account: "+ id);
//		}
		accountRepository.deleteById(id);

	}
}
