package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.Login;
import io.management.repository.LoginRepository;

@Service
public class LoginService {

	@Autowired
	private LoginRepository loginRepository;
	
	public List<Login> findAllLogins(){
//		try {
//			return loginRepository.findAll();
//		} catch (Exception e){
//			throw new RuntimeException("Error fetching all logins", e);
//		}
		return loginRepository.findAll();
	}	
	
	public Login createLogin(Login login) {
//		try {
//			return loginRepository.save(login);
//		}catch (Exception e) {
//			throw new RuntimeException("Error creating login", e);
//		}
		return loginRepository.save(login);

	}
	
	public Login getLoginById(Long id) {
//		try {
//			Login login = loginRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Login not exist with id :" + id));
//			return login;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching login by id", e);
//		}
		Login login = loginRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Login not exist with id :" + id));
		return login;
	}
	
	public Login updateLogin(Long id, Login loginDetails){
//		try {
//			Login login = loginRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Login not exist with id :" + id));
//			
//			login.setUserName(loginDetails.getUserName());
//			login.setPassword(loginDetails.getPassword());
//			
//			Login updatedLogin = loginRepository.save(login);
//			return updatedLogin;
//			
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating login id", e);
//		}
		Login login = loginRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Login not exist with id :" + id));
		
		login.setUserName(loginDetails.getUserName());
		login.setPassword(loginDetails.getPassword());
		
		Login updatedLogin = loginRepository.save(login);
		return updatedLogin;
	}
	
	public void deleteLogin(Long id) {
//		if(!loginRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting login: "+ id);
//		}
		loginRepository.deleteById(id);

	}
}
