package io.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.management.exception.ResourceNotFoundException;
import io.management.model.Platform;
import io.management.repository.PlatformRepository;

@Service
public class PlatformService {

	@Autowired
	private PlatformRepository platformRepository;
	
	public List<Platform> findAllPlatforms(){
//		try {
//			return platformRepository.findAll();
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching all platforms", e);
//		}
		return platformRepository.findAll();
	}	
	
	public Platform createPlatform(Platform platform) {
//		try {
//			return platformRepository.save(platform);
//		} catch (Exception e) {
//			throw new RuntimeException("Error creating platform", e);
//		}
		return platformRepository.save(platform);
	}
	
	public Platform getPlatformById(Long id) {
//		try {
//			Platform platform = platformRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Platform not exist with id :" + id));
//			return platform;
//		} catch (Exception e) {
//			throw new RuntimeException("Error fetching platform by id", e);
//		}
		Platform platform = platformRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Platform not exist with id :" + id));
		return platform;
	}
	
	public Platform updatePlatform(Long id, Platform platformDetails){
//		try {
//			Platform platform = platformRepository.findById(id)
//					.orElseThrow(() -> new ResourceNotFoundException("Platform not exist with id :" + id));
//			
//			platform.setName(platformDetails.getName());
//			
//			Platform updatedPlatform = platformRepository.save(platform);
//			return updatedPlatform;
//		} catch (Exception e) {
//			throw new RuntimeException("Error updating platform id", e);
//		}
		Platform platform = platformRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Platform not exist with id :" + id));
		
		platform.setName(platformDetails.getName());
		
		Platform updatedPlatform = platformRepository.save(platform);
		return updatedPlatform;
	}
	
	public void deletePlatform(Long id) {
//		if(!platformRepository.existsById(id)) {
//			throw new RuntimeException("Error deleting platform: "+ id);
//		}
		platformRepository.deleteById(id);
	}
}
