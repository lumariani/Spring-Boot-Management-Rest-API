package io.management.test.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.management.controller.PlatformController;
import io.management.exception.ResourceNotFoundException;
import io.management.model.Platform;
import io.management.service.PlatformService;

@WebMvcTest(controllers = PlatformController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
public class PlatformControllerTest {

	public static List<Platform> platformList;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
		
	@MockBean
    private PlatformService platformService;
	
	private Platform platform;
	
	 @BeforeEach
	    public void setUp() {
	        platform = Platform.builder().name("Youtube").build();
	        platformList = Stream.of(Platform.builder().name("plat1").build(),
	        		Platform.builder().name("plat2").build(),
	        		Platform.builder().name("plat3").build()
	        		).toList();
	    }

	    @Test
	    public void PlatformController_GetAllPlatforms() throws Exception {
	      	    	
	    	when(platformService.findAllPlatforms()).thenReturn(platformList);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/platforms")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(platformList)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(platformList.get(0).getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(platformList.get(1).getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(platformList.get(2).getName())));
	    }

	    @Test
	    public void PlatformController_CreatePlatform() throws Exception {

	    	given(platformService.createPlatform(ArgumentMatchers.any())).willAnswer((invocation -> invocation.getArgument(0)));
	    	//when(platformService.createPlatform(ArgumentMatchers.any())).thenReturn(platform);
	    	
	    	ResultActions response = mockMvc.perform(post("/academia/v1/platforms")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(platform)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isCreated())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(platform.getName())));	    	
	    }

	    
	    @Test
	    void PlatformController_GetPlatformById() throws Exception {
	    	when(platformService.getPlatformById(platform.getId())).thenReturn(platform);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/platforms/{id}", platform.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(platform)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(platform.getName())))
	    	.andDo(MockMvcResultHandlers.print());	   
	    	
	    }
	    
	    @Test
	    void PlatformController_UpdatePlatform() throws Exception {
	        Platform platform2 = Platform.builder().name("Udemy").build();

	    	when(platformService.updatePlatform(platform.getId(), platform2 )).thenReturn(platform2);

	    	ResultActions response = mockMvc.perform(put("/academia/v1/platforms/{id}", platform2.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(platform2)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(platform2.getName())));	   
	    	
	    }
	    
	    @Test
	    void PlatformController_DeletePlatform() throws Exception {

	        doNothing().when(platformService).deletePlatform(platformList.get(2).getId());
	        

	    	ResultActions response = mockMvc.perform(delete("/academia/v1/platforms/{id}", platformList.get(2).getId())
	    			.contentType(MediaType.APPLICATION_JSON));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isNoContent());	   
	    	
	    }

	    @Test
	    void PlatformController_PlatformNotFound() throws Exception {
	        when(platformService.getPlatformById((long) 444)).thenThrow(new ResourceNotFoundException("Platform not exist with id :" + 444));

	        mockMvc.perform(get("/academia/v1/platforms/{id}", 444))
	                .andExpect(status().isNotFound());
	    }
}
