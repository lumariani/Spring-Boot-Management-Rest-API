package io.management.test.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.management.controller.UserCourseController;
import io.management.exception.ResourceNotFoundException;
import io.management.model.UserCourse;
import io.management.service.UserCourseService;

@WebMvcTest(controllers = UserCourseController.class)
@AutoConfigureMockMvc(addFilters = false)//we dont need to add tokens to our controller
@ExtendWith(MockitoExtension.class)
public class UserCourseControllerTest {

public static List<UserCourse> userCourseList;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
		
	@MockBean
	private UserCourseService userCourseService;
	
	private UserCourse userCourse;
	
	 @BeforeEach
	    public void setUp() {
	        userCourse = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-10-10")).build();
	        userCourseList = Stream.of(UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build(),
	        		UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build(),
	        		UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build()
	        		).toList();
	    }

	    @Test
	    public void UserCourseController_GetAllUserCourses() throws Exception {
	      	    	
	    	when(userCourseService.findAllUserCourses()).thenReturn(userCourseList);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/userCourses")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(userCourseList)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].start", CoreMatchers.is(userCourseList.get(0).getStart().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].end", CoreMatchers.is(userCourseList.get(0).getEnd().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].start", CoreMatchers.is(userCourseList.get(1).getStart().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].end", CoreMatchers.is(userCourseList.get(1).getEnd().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].start", CoreMatchers.is(userCourseList.get(2).getStart().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].end", CoreMatchers.is(userCourseList.get(2).getEnd().toString())));

	    }

	    @Test
	    public void UserCourseController_CreateUserCourse() throws Exception {

	    	given(userCourseService.createUserCourse(ArgumentMatchers.any())).willAnswer((invocation -> invocation.getArgument(0)));
	    	//when(userCourseService.createUserCourse(ArgumentMatchers.any())).thenReturn(userCourse);
	    	
	    	ResultActions response = mockMvc.perform(post("/academia/v1/userCourses")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(userCourse)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isCreated())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.start", CoreMatchers.is(userCourse.getStart().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.end", CoreMatchers.is(userCourse.getEnd().toString())));	   
	    	
	    }

	    
	    @Test
	    void UserCourseController_GetUserCourseById() throws Exception {
	    	when(userCourseService.getUserCourseById(userCourse.getId())).thenReturn(userCourse);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/userCourses/{id}", userCourse.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(userCourse)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.start", CoreMatchers.is(userCourse.getStart().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.end", CoreMatchers.is(userCourse.getEnd().toString())))
	    	.andDo(MockMvcResultHandlers.print());	   
	    	
	    }
	    
	    @Test
	    void UserCourseController_UpdateUserCourse() throws Exception {
	        UserCourse userCourse2 = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build();

	    	when(userCourseService.updateUserCourse(userCourse.getId(), userCourse2 )).thenReturn(userCourse2);

	    	ResultActions response = mockMvc.perform(put("/academia/v1/userCourses/{id}", userCourse2.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(userCourse2)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.start", CoreMatchers.is(userCourse2.getStart().toString())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.end", CoreMatchers.is(userCourse2.getEnd().toString())));	   
	    	
	    }
	    
	    @Test
	    void UserCourseController_DeleteUserCourse() throws Exception {

	        doNothing().when(userCourseService).deleteUserCourse(userCourseList.get(2).getId());
	        

	    	ResultActions response = mockMvc.perform(delete("/academia/v1/userCourses/{id}", userCourseList.get(2).getId())
	    			.contentType(MediaType.APPLICATION_JSON));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isNoContent());	   
	    	
	    }

	    @Test
	    void UserCourseController_UserCourseNotFound() throws Exception {
	        when(userCourseService.getUserCourseById((long) 444)).thenThrow(new ResourceNotFoundException("UserCourse not exist with id :" + 444));

	        mockMvc.perform(get("/academia/v1/userCourses/{id}", 444))
	                .andExpect(status().isNotFound());
	    }
	    
}

