package io.management.test.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.mockito.BDDMockito.given;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.management.controller.AccountController;
import io.management.exception.ResourceNotFoundException;
import io.management.model.Account;
import io.management.service.AccountService;

@WebMvcTest(controllers = AccountController.class)
@AutoConfigureMockMvc(addFilters = false)//we dont need to add tokens to our controller
@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {

	public static List<Account> accountList;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
		
	@MockBean
    private AccountService accountService;
	
	private Account account;
	
	 @BeforeEach
	    public void setUp() {
	        account = Account.builder().userName("luca").password("1234").build();
	        accountList = Stream.of(Account.builder().userName("user1").password("pass1").build(),
	        		Account.builder().userName("user2").password("pass2").build(),
	        		Account.builder().userName("user3").password("pass3").build()
	        		).toList();
	    }

	    @Test
	    public void AccountController_GetAllAccounts() throws Exception {
	      	    	
	    	when(accountService.findAllAccounts()).thenReturn(accountList);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/accounts")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(accountList)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].userName", CoreMatchers.is(accountList.get(0).getUserName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].password", CoreMatchers.is(accountList.get(0).getPassword())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].userName", CoreMatchers.is(accountList.get(1).getUserName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].password", CoreMatchers.is(accountList.get(1).getPassword())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].userName", CoreMatchers.is(accountList.get(2).getUserName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].password", CoreMatchers.is(accountList.get(2).getPassword())));

	    }

	    @Test
	    public void AccountController_CreateAccount() throws Exception {

	    	given(accountService.createAccount(ArgumentMatchers.any())).willAnswer((invocation -> invocation.getArgument(0)));
	    	//when(accountService.createAccount(ArgumentMatchers.any())).thenReturn(account);
	    	
	    	ResultActions response = mockMvc.perform(post("/academia/v1/accounts")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(account)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isCreated())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.userName", CoreMatchers.is(account.getUserName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.password", CoreMatchers.is(account.getPassword())));	   
	    	
	    }

	    
	    @Test
	    void AccountController_GetAccountById() throws Exception {
	    	when(accountService.getAccountById(account.getId())).thenReturn(account);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/accounts/{id}", account.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(account)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.userName", CoreMatchers.is(account.getUserName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.password", CoreMatchers.is(account.getPassword())))
	    	.andDo(MockMvcResultHandlers.print());	   
	    	
	    }
	    
	    @Test
	    void AccountController_UpdateAccount() throws Exception {
	        Account account2 = Account.builder().userName("gigi").password("3333").build();

	    	when(accountService.updateAccount(account.getId(), account2 )).thenReturn(account2);

	    	ResultActions response = mockMvc.perform(put("/academia/v1/accounts/{id}", account2.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(account2)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.userName", CoreMatchers.is(account2.getUserName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.password", CoreMatchers.is(account2.getPassword())));	   
	    	
	    }
	    
	    @Test
	    void AccountController_DeleteAccount() throws Exception {

	        doNothing().when(accountService).deleteAccount(accountList.get(2).getId());
	        

	    	ResultActions response = mockMvc.perform(delete("/academia/v1/accounts/{id}", accountList.get(2).getId())
	    			.contentType(MediaType.APPLICATION_JSON));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isNoContent());	   
	    	
	    }

	    @Test
	    void AccountController_AccountNotFound() throws Exception {
	        when(accountService.getAccountById((long) 444)).thenThrow(new ResourceNotFoundException("Account not exist with id :" + 444));

	        mockMvc.perform(get("/academia/v1/accounts/{id}", 444))
	                .andExpect(status().isNotFound());
	    }
	    
}
