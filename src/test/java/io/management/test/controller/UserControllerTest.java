package io.management.test.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.management.controller.UserController;
import io.management.exception.ResourceNotFoundException;
import io.management.model.User;
import io.management.service.UserService;

@WebMvcTest(controllers = UserController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

	public static List<User> userList;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
		
	@MockBean
    private UserService userService;
	
	private User user;
	
	 @BeforeEach
	    public void setUp() {
	        user = User.builder().email("io@tu.it").build();
	        userList = Stream.of(User.builder().email("gg@hey.com").build(),
	        		User.builder().email("2@mail.it").build(),
	        		User.builder().email("3@mail.com").build()
	        		).toList();
	    }

	    @Test
	    public void UserController_GetAllUsers() throws Exception {
	      	    	
	    	when(userService.findAllUsers()).thenReturn(userList);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/users")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(userList)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].email", CoreMatchers.is(userList.get(0).getEmail())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].email", CoreMatchers.is(userList.get(1).getEmail())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].email", CoreMatchers.is(userList.get(2).getEmail())));
	    }

	    @Test
	    public void UserController_CreateUser() throws Exception {

	    	given(userService.createUser(ArgumentMatchers.any())).willAnswer((invocation -> invocation.getArgument(0)));
	    	//when(userService.createUser(ArgumentMatchers.any())).thenReturn(user);
	    	
	    	ResultActions response = mockMvc.perform(post("/academia/v1/users")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(user)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isCreated())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.is(user.getEmail())));	    	
	    }

	    
	    @Test
	    void UserController_GetUserById() throws Exception {
	    	when(userService.getUserById(user.getId())).thenReturn(user);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/users/{id}", user.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(user)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.is(user.getEmail())))
	    	.andDo(MockMvcResultHandlers.print());	   
	    	
	    }
	    
	    @Test
	    void UserController_UpdateUser() throws Exception {
	        User user2 = User.builder().email("nome@mail.it").build();

	    	when(userService.updateUser(user.getId(), user2 )).thenReturn(user2);

	    	ResultActions response = mockMvc.perform(put("/academia/v1/users/{id}", user2.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(user2)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.is(user2.getEmail())));	   
	    	
	    }
	    
	    @Test
	    void UserController_DeleteUser() throws Exception {

	        doNothing().when(userService).deleteUser(userList.get(2).getId());
	        

	    	ResultActions response = mockMvc.perform(delete("/academia/v1/users/{id}", userList.get(2).getId())
	    			.contentType(MediaType.APPLICATION_JSON));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isNoContent());	   
	    	
	    }

	    @Test
	    void UserController_UserNotFound() throws Exception {
	        when(userService.getUserById((long) 444)).thenThrow(new ResourceNotFoundException("User not exist with id :" + 444));

	        mockMvc.perform(get("/academia/v1/users/{id}", 444))
	                .andExpect(status().isNotFound());
	    }
}
