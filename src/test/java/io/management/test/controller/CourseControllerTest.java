package io.management.test.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.management.controller.CourseController;
import io.management.exception.ResourceNotFoundException;
import io.management.model.Course;
import io.management.service.CourseService;

@WebMvcTest(controllers = CourseController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
public class CourseControllerTest {

	public static List<Course> courseList;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
		
	@MockBean
    private CourseService courseService;
	
	private Course course;
	
	 @BeforeEach
	    public void setUp() {
	        course = Course.builder().id(1L).name("luca").language("1234").build();
	        courseList = Stream.of(Course.builder().name("user1").language("pass1").build(),
	        		Course.builder().id(1L).name("user2").language("pass2").build(),
	        		Course.builder().id(2L).name("user3").language("pass3").build(),
	        		Course.builder().id(3L).name("user3").language("pass3").build()
	        		).toList();
	    }

	    @Test
	    public void CourseController_GetAllCourses() throws Exception {
	      	    	
	    	when(courseService.findAllCourses()).thenReturn(courseList);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/courses")
	    			.contentType(MediaType.APPLICATION_JSON));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.size()", CoreMatchers.is(courseList.size())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(courseList.get(0).getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[0].language", CoreMatchers.is(courseList.get(0).getLanguage())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(courseList.get(1).getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[1].language", CoreMatchers.is(courseList.get(1).getLanguage())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(courseList.get(2).getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$[2].language", CoreMatchers.is(courseList.get(2).getLanguage())));

	    }

	    @Test
	    public void CourseController_CreateCourse() throws Exception {

	    	given(courseService.createCourse(ArgumentMatchers.any())).willAnswer((invocation -> invocation.getArgument(0)));
	    	//when(courseService.createCourse(ArgumentMatchers.any())).thenReturn(course);
	    	
	    	ResultActions response = mockMvc.perform(post("/academia/v1/courses")
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(course)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isCreated())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(course.getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.language", CoreMatchers.is(course.getLanguage())));	   
	    	
	    }

	    
	    @Test
	    void CourseController_GetCourseById() throws Exception {
	    	when(courseService.getCourseById(course.getId())).thenReturn(course);

	    	ResultActions response = mockMvc.perform(get("/academia/v1/courses/{id}", course.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(course)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(course.getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.language", CoreMatchers.is(course.getLanguage())))
	    	.andDo(MockMvcResultHandlers.print());	   
	    	
	    }
	    
	    @Test
	    void CourseController_UpdateCourse() throws Exception {
	        Course course2 = Course.builder().name("gigi").language("3333").build();

	    	when(courseService.updateCourse(course.getId(), course2 )).thenReturn(course2);

	    	ResultActions response = mockMvc.perform(put("/academia/v1/courses/{id}", course.getId())
	    			.contentType(MediaType.APPLICATION_JSON)
	    			.content(objectMapper.writeValueAsString(course2)));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isOk())
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(course2.getName())))
	    	.andExpect(MockMvcResultMatchers.jsonPath("$.language", CoreMatchers.is(course2.getLanguage())));	   
	    	
	    }
	    
	    @Test
	    void CourseController_DeleteCourse() throws Exception {

	        doNothing().when(courseService).deleteCourse(courseList.get(2).getId());
	        

	    	ResultActions response = mockMvc.perform(delete("/academia/v1/courses/{id}", courseList.get(2).getId())
	    			.contentType(MediaType.APPLICATION_JSON));
	    	
	    	response.andExpect(MockMvcResultMatchers.status().isNoContent());	   
	    	
	    }

	    @Test
	    void CourseController_CourseNotFound() throws Exception {
	        when(courseService.getCourseById((long) 444)).thenThrow(new ResourceNotFoundException("Course not exist with id :" + 444));

	        mockMvc.perform(get("/academia/v1/courses/{id}", 444))
	                .andExpect(status().isNotFound());
	    }
}
