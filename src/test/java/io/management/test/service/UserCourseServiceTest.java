package io.management.test.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import io.management.model.UserCourse;
import io.management.repository.UserCourseRepository;
import io.management.service.UserCourseService;

@ExtendWith(MockitoExtension.class)
public class UserCourseServiceTest {

	@Mock
	private UserCourseRepository userCourseRepository;
	
	@InjectMocks
	private UserCourseService userCourseService;
	
	UserCourse userCourse;
	
	@BeforeEach
	public void init() {
		userCourse = UserCourse.builder().start(LocalDate.parse("2024-11-11")).end(LocalDate.parse("2024-11-21")).build();

	}
	@Test
	public void UserCourseService_CreateUserCourse() {

		when(userCourseRepository.save(Mockito.any(UserCourse.class))).thenReturn(userCourse);
		
		UserCourse savedUserCourse = userCourseService.createUserCourse(userCourse);
		
		Assertions.assertThat(savedUserCourse).isNotNull();
		verify(userCourseRepository, times(1)).save(userCourse);
	}
	
	@Test
	public void UserCourseService_FindAll() {

		UserCourse userCourse2 = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build();
		
		when(userCourseRepository.findAll()).thenReturn(List.of(userCourse, userCourse2));
		
		List<UserCourse> savedUserCourse = userCourseService.findAllUserCourses();
		
		Assertions.assertThat(savedUserCourse).isNotNull();
		Assertions.assertThat(savedUserCourse).hasSize(2);
	    verify(userCourseRepository, times(1)).findAll();
	}
	
	@Test
	public void UserCourseService_GetUserCourseById() {

		
		when(userCourseRepository.findById((long) 1)).thenReturn(Optional.ofNullable(userCourse));
		
		UserCourse foundUserCourse = userCourseService.getUserCourseById((long) 1);
		
		Assertions.assertThat(foundUserCourse).isNotNull();
		verify(userCourseRepository, times(1)).findById(1L);
	}
	
	@Test
	public void UserCourseService_UpdateUserCourse() {
		UserCourse existingUserCourse = userCourse;
        UserCourse updatedUserCourseDetails = UserCourse.builder().start(LocalDate.parse("2024-02-02")).end(LocalDate.parse("2024-02-02")).build();

        when(userCourseRepository.findById(1L)).thenReturn(Optional.of(existingUserCourse));
        when(userCourseRepository.save(Mockito.any(UserCourse.class))).thenReturn(updatedUserCourseDetails);

        UserCourse updatedUserCourse = userCourseService.updateUserCourse(1L, updatedUserCourseDetails);

        Assertions.assertThat(updatedUserCourse).isNotNull();
        Assertions.assertThat(updatedUserCourse.getStart()).isEqualTo(LocalDate.parse("2024-02-02"));
        Assertions.assertThat(updatedUserCourse.getEnd()).isEqualTo(LocalDate.parse("2024-02-02"));
        verify(userCourseRepository, times(1)).findById(1L);
        verify(userCourseRepository, times(1)).save(existingUserCourse);
	}
	
	@Test
	public void UserCourseService_DeleteUserCourse() {
		
		 doNothing().when(userCourseRepository).deleteById(1L);

	        userCourseService.deleteUserCourse(1L);

	        verify(userCourseRepository, times(1)).deleteById(1L);
	}
}
