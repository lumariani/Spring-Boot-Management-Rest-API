package io.management.test.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import io.management.model.User;
import io.management.repository.UserRepository;
import io.management.service.UserService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	@Mock
	private UserRepository userRepository;
	
	@InjectMocks
	private UserService userService;
	
	private User user;
	
	@BeforeEach
	public void init() {
		user = User.builder().id(1).email("email1").build();

	}
	
	@Test
	public void UserService_CreateUser() {		
		when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
		
		User savedUser = userService.createUser(user);
		
		Assertions.assertThat(savedUser).isNotNull();
		verify(userRepository, times(1)).save(user);
	}
	
	@Test
	public void UserService_FindAll() {
		User user2 = User.builder().email("email2").build();

		
		when(userRepository.findAll()).thenReturn(List.of(user, user2));
		
		List<User> savedUser = userService.findAllUsers();
		
		Assertions.assertThat(savedUser).isNotNull();
		Assertions.assertThat(savedUser).hasSize(2);
	    verify(userRepository, times(1)).findAll();
	}
	
	@Test
	public void UserService_GetUserById() {
		
		//when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
		
		when(userRepository.findById((long) 1)).thenReturn(Optional.ofNullable(user));
		
		User savedUser = userService.getUserById((long) 1);
		
		Assertions.assertThat(savedUser).isNotNull();
		verify(userRepository, times(1)).findById(1L);
	}
	
	@Test
	public void UserService_UpdateUser() {
		
		User existingUser = user;
        User updatedUserDetails = User.builder().email("updatedEmail").build();

        when(userRepository.findById(1L)).thenReturn(Optional.of(existingUser));
        when(userRepository.save(Mockito.any(User.class))).thenReturn(updatedUserDetails);

        User updatedUser = userService.updateUser(1L, updatedUserDetails);

        Assertions.assertThat(updatedUser).isNotNull();
        Assertions.assertThat(updatedUser.getEmail()).isEqualTo("updatedEmail");
        verify(userRepository, times(1)).findById(1L);
        verify(userRepository, times(1)).save(existingUser);
	}
	
	@Test
	public void UserService_DeleteUser() {
		
		 doNothing().when(userRepository).deleteById(1L);

	        userService.deleteUser(1L);

	        verify(userRepository, times(1)).deleteById(1L);
	}
}
