package io.management.test.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import io.management.model.Platform;
import io.management.repository.PlatformRepository;
import io.management.service.PlatformService;

@ExtendWith(MockitoExtension.class)
public class PlatformServiceTest {
	
	@Mock
	private PlatformRepository platformRepository;
	
	@InjectMocks
	private PlatformService platformService;
	
	private Platform platform;
	
	@BeforeEach
	public void init() {
		platform = Platform.builder().name("name1").build();

	}
	
	@Test
	public void PlatformService_CreatePlatform() {		
		when(platformRepository.save(Mockito.any(Platform.class))).thenReturn(platform);
		
		Platform savedPlatform = platformService.createPlatform(platform);
		
		Assertions.assertThat(savedPlatform).isNotNull();
		verify(platformRepository, times(1)).save(platform);
	}
	
	@Test
	public void PlatformService_FindAll() {
		Platform platform2 = Platform.builder().name("name2").build();
		
		when(platformRepository.findAll()).thenReturn(List.of(platform, platform2));
		
		List<Platform> savedPlatform = platformService.findAllPlatforms();
		
		Assertions.assertThat(savedPlatform).isNotNull();
		Assertions.assertThat(savedPlatform).hasSize(2);
	    verify(platformRepository, times(1)).findAll();
	}
	
	@Test
	public void PlatformService_GetPlatformById() {
		
		//when(platformRepository.save(Mockito.any(Platform.class))).thenReturn(platform);
		
		when(platformRepository.findById((long) 1)).thenReturn(Optional.ofNullable(platform));
		
		Platform savedPlatform = platformService.getPlatformById((long) 1);
		
		Assertions.assertThat(savedPlatform).isNotNull();
		verify(platformRepository, times(1)).findById(1L);
	}
	
	@Test
	public void PlatformService_UpdatePlatform() {
		
		Platform existingPlatform = platform;
        Platform updatedPlatformDetails = Platform.builder().name("updatedName").build();

        when(platformRepository.findById(1L)).thenReturn(Optional.of(existingPlatform));
        when(platformRepository.save(Mockito.any(Platform.class))).thenReturn(updatedPlatformDetails);

        Platform updatedPlatform = platformService.updatePlatform(1L, updatedPlatformDetails);

        Assertions.assertThat(updatedPlatform).isNotNull();
        Assertions.assertThat(updatedPlatform.getName()).isEqualTo("updatedName");
        verify(platformRepository, times(1)).findById(1L);
        verify(platformRepository, times(1)).save(existingPlatform);
	}
	
	@Test
	public void PlatformService_DeletePlatform() {
		
		doNothing().when(platformRepository).deleteById(1L);

        platformService.deletePlatform(1L);

        verify(platformRepository, times(1)).deleteById(1L);
	}

}
