package io.management.test.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import io.management.model.Account;
import io.management.repository.AccountRepository;
import io.management.service.AccountService;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

	@Mock
	private AccountRepository accountRepository;
	
	@InjectMocks
	private AccountService accountService;
	
	Account account;
	
	@BeforeEach
	public void init() {
		account = Account.builder().userName("user1").password("pass1").build();

	}
	@Test
	public void AccountService_CreateAccount() {

		when(accountRepository.save(Mockito.any(Account.class))).thenReturn(account);
		
		Account savedAccount = accountService.createAccount(account);
		
		Assertions.assertThat(savedAccount).isNotNull();
		verify(accountRepository, times(1)).save(account);
	}
	
	@Test
	public void AccountService_FindAll() {

		Account account2 = Account.builder().userName("user2").password("pass2").build();
		
		when(accountRepository.findAll()).thenReturn(List.of(account, account2));
		
		List<Account> savedAccount = accountService.findAllAccounts();
		
		Assertions.assertThat(savedAccount).isNotNull();
		Assertions.assertThat(savedAccount).hasSize(2);
	    verify(accountRepository, times(1)).findAll();
	}
	
	@Test
	public void AccountService_GetAccountById() {

		
		when(accountRepository.findById((long) 1)).thenReturn(Optional.ofNullable(account));
		
		Account foundAccount = accountService.getAccountById((long) 1);
		
		Assertions.assertThat(foundAccount).isNotNull();
		verify(accountRepository, times(1)).findById(1L);
	}
	
	@Test
	public void AccountService_UpdateAccount() {
		Account existingAccount = account;
        Account updatedAccountDetails = Account.builder().userName("updatedUser").password("updatedPass").build();

        when(accountRepository.findById(1L)).thenReturn(Optional.of(existingAccount));
        when(accountRepository.save(Mockito.any(Account.class))).thenReturn(updatedAccountDetails);

        Account updatedAccount = accountService.updateAccount(1L, updatedAccountDetails);

        Assertions.assertThat(updatedAccount).isNotNull();
        Assertions.assertThat(updatedAccount.getUserName()).isEqualTo("updatedUser");
        Assertions.assertThat(updatedAccount.getPassword()).isEqualTo("updatedPass");
        verify(accountRepository, times(1)).findById(1L);
        verify(accountRepository, times(1)).save(existingAccount);
	}
	
	@Test
	public void AccountService_DeleteAccount() {
		
		 doNothing().when(accountRepository).deleteById(1L);

	        accountService.deleteAccount(1L);

	        verify(accountRepository, times(1)).deleteById(1L);
	}
	
	
}
