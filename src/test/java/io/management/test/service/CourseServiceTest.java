package io.management.test.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import io.management.model.Course;
import io.management.repository.CourseRepository;
import io.management.service.CourseService;

@ExtendWith(MockitoExtension.class)
public class CourseServiceTest {

	@Mock
	private CourseRepository courseRepository;
	
	@InjectMocks
	private CourseService courseService;
	
	private Course course;
	
	@BeforeEach
	public void init() {
		course = Course.builder().name("name1").language("lang1").build();

	}
	
	@Test
	public void CourseService_CreateCourse() {		
		when(courseRepository.save(Mockito.any(Course.class))).thenReturn(course);
		
		Course savedCourse = courseService.createCourse(course);
		
		Assertions.assertThat(savedCourse).isNotNull();
		verify(courseRepository, times(1)).save(course);
	}
	
	@Test
	public void CourseService_FindAll() {
		Course course2 = Course.builder().name("name2").language("language2").build();
		
		when(courseRepository.findAll()).thenReturn(List.of(course, course2));
		
		List<Course> savedCourse = courseService.findAllCourses();
		
		Assertions.assertThat(savedCourse).isNotNull();
		Assertions.assertThat(savedCourse).hasSize(2);
		verify(courseRepository, times(1)).findAll();
	}
	
	@Test
	public void CourseService_GetCourseById() {
		
		//when(courseRepository.save(Mockito.any(Course.class))).thenReturn(course);
		
		when(courseRepository.findById((long) 1)).thenReturn(Optional.ofNullable(course));
		
		Course savedCourse = courseService.getCourseById((long) 1);
		
		Assertions.assertThat(savedCourse).isNotNull();
		verify(courseRepository, times(1)).findById(1L);
	}
	
	@Test
	public void CourseService_UpdateCourse() {
		
		Course existingCourse = course;
        Course updatedCourseDetails = Course.builder().name("updatedName").language("updatedLang").build();

        when(courseRepository.findById(1L)).thenReturn(Optional.of(existingCourse));
        when(courseRepository.save(Mockito.any(Course.class))).thenReturn(updatedCourseDetails);

        Course updatedCourse = courseService.updateCourse(1L, updatedCourseDetails);

        Assertions.assertThat(updatedCourse).isNotNull();
        Assertions.assertThat(updatedCourse.getName()).isEqualTo("updatedName");
        Assertions.assertThat(updatedCourse.getLanguage()).isEqualTo("updatedLang");
        verify(courseRepository, times(1)).findById(1L);
        verify(courseRepository, times(1)).save(existingCourse);
	}
	
	@Test
	public void CourseService_DeleteCourse() {
		
		doNothing().when(courseRepository).deleteById(1L);

        courseService.deleteCourse(1L);

        verify(courseRepository, times(1)).deleteById(1L);	
        
	}
	
}
