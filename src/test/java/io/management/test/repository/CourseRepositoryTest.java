//package io.management.test.repository;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import io.management.model.Course;
//import io.management.repository.CourseRepository;
//
//@DataJpaTest
//public class CourseRepositoryTest {
//
//	@Autowired
//	private CourseRepository courseRepository;
//	
//	@Test
//	public void CourseRepository_SaveAll() {
//		//Arrange
//		Course course = Course.builder().name("name1").language("lang1").build();
//		//Course course = new Course(1, "name1", "lang1");
//		
//		//Act
//		Course savedCourse = courseRepository.save(course);
//		
//		//Assert
//		Assertions.assertThat(savedCourse).isNotNull();
//		Assertions.assertThat(savedCourse.getId()).isGreaterThan(0);
//	}
//	
//	@Test
//	public void CourseRepository_FindAll() {
//		//Arrange
//		Course course = Course.builder().name("name1").language("lang1").build();
//		Course course2 = Course.builder().name("name2").language("lang2").build();
//
//		//Act
//		courseRepository.save(course);
//		courseRepository.save(course2);
//		List<Course> courseList = courseRepository.findAll();
//
//		//Assert
//		
//		Assertions.assertThat(courseList).isNotNull();
//		Assertions.assertThat(courseList.size()).isEqualTo(2);
//	}
//	
//	@Test
//	public void CourseRepository_FindById() {
//		//Arrange
//		Course course = Course.builder().name("name1").language("lang1").build();
//
//		//Act
//		courseRepository.save(course);
//		Course courseReturn = courseRepository.findById(course.getId()).get();
//
//		//Assert
//		
//		Assertions.assertThat(courseReturn).isNotNull();
//	}
//	
//	@Test
//	public void CourseRepository_Update() {
//		//Arrange
//		Course course = Course.builder().name("name1").language("lang1").build();
//		
//		courseRepository.save(course);
//		
//		Course courseSave = courseRepository.findById(course.getId()).get();
//		courseSave.setName("Gianni");
//		courseSave.setLanguage("English");
//
//		//Act
//		Course updateCourse = courseRepository.save(courseSave);
//
//		//Assert
//		Assertions.assertThat(updateCourse.getName()).isNotNull();
//		Assertions.assertThat(updateCourse.getLanguage()).isNotNull();
//
//	}
//	
//	@Test
//	public void CourseRepository_DeleteById() {
//		//Arrange
//		Course course = Course.builder().name("name1").language("lang1").build();
//
//		//Act
//		courseRepository.save(course);
//		courseRepository.deleteById(course.getId());
//
//		Optional<Course> courseReturn = courseRepository.findById(course.getId());
//		//Assert
//		Assertions.assertThat(courseReturn).isEmpty();
//	}
//	
//}
