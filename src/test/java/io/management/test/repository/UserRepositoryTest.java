//package io.management.test.repository;
//
//import java.time.LocalDate;
//import java.util.List;
//import java.util.Optional;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
////import io.management.model.Course;
//import io.management.model.User;
//import io.management.repository.UserRepository;
//
//@DataJpaTest
//public class UserRepositoryTest {
//
//	@Autowired
//	private UserRepository userRepository;
//	
//	@Test
//	public void UserRepository_SaveAll() {
//		//Arrange
//		//Course course = Course.builder().name("name1").language("lang1").build();
//		User user = User.builder().email("mail@1.com").build();
//		//User user = new User(1, "user1", "pass1");
//		
//		//Act
//		User savedUser = userRepository.save(user);
//		
//		//Assert
//		Assertions.assertThat(savedUser).isNotNull();
//		Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
//	}
//	
//	@Test
//	public void UserRepository_FindAll() {
//		//Arrange
//		User user = User.builder().email("mail@1.com").build();
//		User user2 = User.builder().email("mail2@io.it").build();
//
//		//Act
//		userRepository.save(user);
//		userRepository.save(user2);
//		List<User> userList = userRepository.findAll();
//
//		//Assert
//		
//		Assertions.assertThat(userList).isNotNull();
//		Assertions.assertThat(userList.size()).isEqualTo(2);
//	}
//	
//	@Test
//	public void UserRepository_FindById() {
//		//Arrange
//		User user = User.builder().email("user1").build();
//
//		//Act
//		userRepository.save(user);
//		User userReturn = userRepository.findById(user.getId()).get();
//
//		//Assert
//		
//		Assertions.assertThat(userReturn).isNotNull();
//	}
//	
//	@Test
//	public void UserRepository_Update() {
//		//Arrange
//		User user = User.builder().email("mail@1.com").build();
//		
//		userRepository.save(user);
//		
//		User userSave = userRepository.findById(user.getId()).get();
//		userSave.setEmail("mail@bot.com");
//		//userSave.setCourse(null);
//		
//
//		//Act
//		User updateUser = userRepository.save(userSave);
//
//		//Assert
//		Assertions.assertThat(updateUser.getEmail()).isNotNull();
//
//	}
//	
//	@Test
//	public void UserRepository_DeleteById() {
//		//Arrange
//		User user = User.builder().email("mail@1.com").build();
//
//		//Act
//		userRepository.save(user);
//		userRepository.deleteById(user.getId());
//
//		Optional<User> userReturn = userRepository.findById(user.getId());
//		//Assert
//		Assertions.assertThat(userReturn).isEmpty();
//	}
//	
//}
