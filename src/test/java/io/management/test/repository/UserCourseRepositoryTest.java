package io.management.test.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import io.management.model.Course;
import io.management.model.User;
import io.management.model.UserCourse;
import io.management.repository.UserCourseRepository;

@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserCourseRepositoryTest {

//	@Autowired
//	private UserCourseRepository userCourseRepository;
//	
//	private UserCourse userCourse, userCourse2;
//	private User user, user2;
//	private Course course, course2;
//	
//	@BeforeEach
//	public void init() {
//		user = User.builder().email("mail@1.com").build();
//		user2 = User.builder().email("mail@bot.com").build();
//		course = Course.builder().name("name1").language("lang1").build();
//		course2 = Course.builder().name("name2").language("lang2").build();
//
//	 userCourse = UserCourse.
//				builder().end(LocalDate.parse("2024-01-01")).start(LocalDate.parse("2024-01-01"))
//				.course(course).user(user).build();
//	 
//	 userCourse2 = UserCourse.builder().start(LocalDate.parse("2024-01-01"))
//			 .end(LocalDate.parse("2024-01-01")).user(user2).course(course2).build();
//	}
//	
//	@Test
//	public void UserCourseRepository_SaveAll() {
//		//Arrange
//		/*
//		 * UserCourse userCourse = UserCourse.
//		 * builder().start(LocalDate.parse("2024-01-01"))
//		 * .end(LocalDate.parse("2024-01-01")).user(null).course(null).build();
//		 */
//		//UserCourse userCourse3 = new UserCourse(1L, LocalDate.parse("2024-01-01"),LocalDate.parse("2024-01-01"), user, course);
//		
//		//Act
//		try {
//		UserCourse savedUserCourse = userCourseRepository.save(userCourse);
//		Assertions.assertThat(savedUserCourse).isNotNull();
//		Assertions.assertThat(savedUserCourse.getId()).isGreaterThan(0);}
//		catch(Exception e) {
//			throw new RuntimeException("Error creating userCourse", e);
//		}
//		
//		//Assert
////		Assertions.assertThat(savedUserCourse).isNotNull();
////		Assertions.assertThat(savedUserCourse.getId()).isGreaterThan(0);
//	}
//	
//	@Test
//	public void UserCourseRepository_FindAll() {
//		//Arrange
////		UserCourse userCourse = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build();
////		UserCourse userCourse2 = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build();
//
//		//Act
//		userCourseRepository.save(userCourse);
//		userCourseRepository.save(userCourse2);
//		List<UserCourse> userCourseList = userCourseRepository.findAll();
//
//		//Assert
//		
//		Assertions.assertThat(userCourseList).isNotNull();
//		Assertions.assertThat(userCourseList.size()).isEqualTo(2);
//	}
//	
//	@Test
//	public void UserCourseRepository_FindById() {
//		//Arrange
////		UserCourse userCourse = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build();
//
//		//Act
//		userCourseRepository.save(userCourse);
//		UserCourse userCourseReturn = userCourseRepository.findById(userCourse.getId()).get();
//
//		//Assert
//		
//		Assertions.assertThat(userCourseReturn).isNotNull();
//	}
//	
//	@Test
//	public void UserCourseRepository_Update() {
//		//Arrange
////		UserCourse userCourse = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-01-01")).build();
//		
//		userCourseRepository.save(userCourse);
//		
//		UserCourse userCourseSave = userCourseRepository.findById(userCourse.getId()).get();
//		userCourseSave.setStart(LocalDate.parse("2024-09-01"));
//		userCourseSave.setEnd(LocalDate.parse("2024-11-01"));
//
//		//Act
//		UserCourse updateUserCourse = userCourseRepository.save(userCourseSave);
//
//		//Assert
//		Assertions.assertThat(updateUserCourse.getStart()).isNotNull();
//		Assertions.assertThat(updateUserCourse.getEnd()).isNotNull();
//
//	}
//	
//	@Test
//	public void UserCourseRepository_DeleteById() {
//		//Arrange
////		UserCourse userCourse = UserCourse.builder().start(LocalDate.parse("2024-01-01")).end(LocalDate.parse("2024-02-02")).build();
//
//		//Act
//		userCourseRepository.save(userCourse);
//		userCourseRepository.deleteById(userCourse.getId());
//
//		Optional<UserCourse> userCourseReturn = userCourseRepository.findById(userCourse.getId());
//		//Assert
//		Assertions.assertThat(userCourseReturn).isEmpty();
//	}
}
