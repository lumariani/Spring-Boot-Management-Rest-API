//package io.management.test.repository;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import io.management.model.Account;
//import io.management.repository.AccountRepository;
//
//@DataJpaTest
////@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
//public class AccountRepositoryTest {
//
//	@Autowired
//	private AccountRepository accountRepository;
//	
//	@Test
//	public void AccountRepository_SaveAll() {
//		//Arrange
//		Account account = Account.
//				builder().userName("user1")
//				.password("pass1").build();
//		//Account account = new Account(1, "user1", "pass1");
//		
//		//Act
//		Account savedAccount = accountRepository.save(account);
//		
//		//Assert
//		Assertions.assertThat(savedAccount).isNotNull();
//		Assertions.assertThat(savedAccount.getId()).isGreaterThan(0);
//	}
//	
//	@Test
//	public void AccountRepository_FindAll() {
//		//Arrange
//		Account account = Account.builder().userName("user1").password("pass1").build();
//		Account account2 = Account.builder().userName("user2").password("pass2").build();
//
//		//Act
//		accountRepository.save(account);
//		accountRepository.save(account2);
//		List<Account> accountList = accountRepository.findAll();
//
//		//Assert
//		
//		Assertions.assertThat(accountList).isNotNull();
//		Assertions.assertThat(accountList.size()).isEqualTo(2);
//	}
//	
//	@Test
//	public void AccountRepository_FindById() {
//		//Arrange
//		Account account = Account.builder().userName("user1").password("pass1").build();
//
//		//Act
//		accountRepository.save(account);
//		Account accountReturn = accountRepository.findById(account.getId()).get();
//
//		//Assert
//		
//		Assertions.assertThat(accountReturn).isNotNull();
//	}
//	
//	@Test
//	public void AccountRepository_Update() {
//		//Arrange
//		Account account = Account.builder().userName("user1").password("pass1").build();
//		
//		accountRepository.save(account);
//		
//		Account accountSave = accountRepository.findById(account.getId()).get();
//		accountSave.setUserName("gino");
//		accountSave.setPassword("123");
//
//		//Act
//		Account updateAccount = accountRepository.save(accountSave);
//
//		//Assert
//		Assertions.assertThat(updateAccount.getUserName()).isNotNull();
//		Assertions.assertThat(updateAccount.getPassword()).isNotNull();
//
//	}
//	
//	@Test
//	public void AccountRepository_DeleteById() {
//		//Arrange
//		Account account = Account.builder().userName("user1").password("pass1").build();
//
//		//Act
//		accountRepository.save(account);
//		accountRepository.deleteById(account.getId());
//
//		Optional<Account> accountReturn = accountRepository.findById(account.getId());
//		//Assert
//		Assertions.assertThat(accountReturn).isEmpty();
//	}
//	
//	
//	
//}
