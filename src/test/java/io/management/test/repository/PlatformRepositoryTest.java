//package io.management.test.repository;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import io.management.model.Platform;
//import io.management.repository.PlatformRepository;
//
//@DataJpaTest
//public class PlatformRepositoryTest {
//
//	@Autowired
//	private PlatformRepository platformRepository;
//	
//	@Test
//	public void PlatformRepository_SaveAll() {
//		//Arrange
//		Platform platform = Platform.builder().name("name1").build();
//		//Platform platform = new Platform(1, "name1");
//		//Act
//		Platform savedPlatform = platformRepository.save(platform);
//		
//		//Assert
//		Assertions.assertThat(savedPlatform).isNotNull();
//		Assertions.assertThat(savedPlatform.getId()).isGreaterThan(0);
//	}
//	
//	@Test
//	public void PlatformRepository_FindAll() {
//		//Arrange
//		Platform platform = Platform.builder().name("name1").build();
//		Platform platform2 = Platform.builder().name("name2").build();
//
//		//Act
//		platformRepository.save(platform);
//		platformRepository.save(platform2);
//		List<Platform> platformList = platformRepository.findAll();
//
//		//Assert
//		
//		Assertions.assertThat(platformList).isNotNull();
//		Assertions.assertThat(platformList.size()).isEqualTo(2);
//	}
//	
//	@Test
//	public void PlatformRepository_FindById() {
//		//Arrange
//		Platform platform = Platform.builder().name("name1").build();
//
//		//Act
//		platformRepository.save(platform);
//		Platform platformReturn = platformRepository.findById(platform.getId()).get();
//
//		//Assert
//		
//		Assertions.assertThat(platformReturn).isNotNull();
//	}
//	
//	@Test
//	public void PlatformRepository_Update() {
//		//Arrange
//		Platform platform = Platform.builder().name("name1").build();
//		
//		//Act
//		platformRepository.save(platform);
//		
//		Platform platformSave = platformRepository.findById(platform.getId()).get();
//		platformSave.setName("Youtube");
//
//		Platform updatePlatform = platformRepository.save(platformSave);
//
//		//Assert
//		Assertions.assertThat(updatePlatform.getName()).isNotNull();
//
//	}
//	
//	@Test
//	public void PlatformRepository_DeleteById() {
//		//Arrange
//		Platform platform = Platform.builder().name("name1").build();
//
//		//Act
//		platformRepository.save(platform);
//		platformRepository.deleteById(platform.getId());
//
//		Optional<Platform> platformReturn = platformRepository.findById(platform.getId());
//		//Assert
//		Assertions.assertThat(platformReturn).isEmpty();
//	}
//}
